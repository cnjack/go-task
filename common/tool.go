package common

import (
	"git.oschina.net/cnjack/go-task/log"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"

	"io/ioutil"
	"net"
	"net/http"
	"time"
)

func DoGetTask(task string) {
	c := http.Client{
		Transport: &http.Transport{
			Dial: func(netw, addr string) (net.Conn, error) {
				deadline := time.Now().Add(25 * time.Second)
				c, err := net.DialTimeout(netw, addr, time.Second*20)
				if err != nil {
					return nil, err
				}
				c.SetDeadline(deadline)
				return c, nil
			},
		},
	}
	resp, err := c.Get("task")
	//resp, err := http.Get(task)
	if err != nil {
		log.WarnNoStyle(err.Error())
	}
	if resp.StatusCode == 200 {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.WarnNoStyle(err.Error())
		}
		log.InfoNoStyle(string(body))
	}
}

func DoMySql(task string) {
	engine, _ := xorm.NewEngine("mysql", "root:root@tcp(127.0.0.1:3306)/fqds?charset=utf8")
	engine.ShowSQL()
	err := engine.DumpAllToFile("./sql.sql")
	if err != nil {
		log.WarnNoStyle(err.Error())
	}
	log.WarnNoStyle(task + "备份成功")
	Upfile("./sql.sql", "sql.sql", "fqd360")
	log.WarnNoStyle(task + "上传成功")
}
