package common

import (
	"git.oschina.net/cnjack/go-task/log"
	//"github.com/Unknwon/goconfig"
	"github.com/qiniu/api.v6/conf"
	"github.com/qiniu/api.v6/io"
	"github.com/qiniu/api.v6/rs"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func GetCurrPath() string {
	file, _ := exec.LookPath(os.Args[0])
	path, _ := filepath.Abs(file)
	splitstring := strings.Split(path, "\\")
	size := len(splitstring)
	splitstring = strings.Split(path, splitstring[size-1])
	ret := strings.Replace(splitstring[0], "\\", "/", size-1)
	return ret
}
func Upfile(filePath, filename, b string) {
	conf.ACCESS_KEY = ""
	conf.SECRET_KEY = ""
	token := uptoken(b, filename)
	var err error
	var ret io.PutRet
	var extra = &io.PutExtra{}
	err = io.PutFile(nil, &ret, token, filename, filePath, extra)

	if err != nil {
		//上传产生错误
		log.WarnNoStyle("io.PutFile failed:", err)
		return
	}
}
func uptoken(bucketName, filename string) string {
	putPolicy := rs.PutPolicy{
		Scope: bucketName + ":" + filename,
	}
	return putPolicy.Token(nil)
}
