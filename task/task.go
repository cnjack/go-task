package task

import (
	"git.oschina.net/cnjack/go-task/common"
	"sync"
	"time"
)

type Task struct {
	Id          int    //任务id
	Time        int64  //任务执行时间或者时间间隔
	TaskType    int    //任务的类型
	TaskVaule   string //任务
	NextRunTime int64  //下次运行时间
	Lock        sync.Mutex
}

func (this *Task) Update() {
	this.NextRunTime = time.Now().Unix() + this.Time
}

func (this *Task) Do() Task {
	this.Lock.Lock()
	if this.TaskType == 1 {
		common.DoGetTask(this.TaskVaule)
	}
	switch this.TaskType {
	case 1:
		common.DoGetTask(this.TaskVaule)
		break
	case 2:
		common.DoMySql(this.TaskVaule)
		break
	}
	this.Update()
	this.Lock.Unlock()
	return *this
}
