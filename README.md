#go-task
go-task is a task queue(just visit a url by GET) writed by GoLang   
go-task 是一个任务列表 目前支持定时访问一个url,通过golang编写
## author
cnjack -- h_7357#qq.com
## how to run
    go get git.oschina.net/cnjack/go-task   
    
    cd $GOPATH/src/git.oschina.net/cnjack/go-task   
    or %GOPATH%/src/git.oschina.net/cnjack/go-task(win)   
    
    go build
    ./go-task(linux) OR go-task(win)

    visit http://127.0.0.1:3201/
    click "add task" to add task
    click "view the task" to see the taskList
    the default pass is 3201
    (add pass to avoid some strange events)
## 如何运行
    go get git.oschina.net/cnjack/go-task   
    
    cd $GOPATH/src/git.oschina.net/cnjack/go-task   
    or %GOPATH%/src/git.oschina.net/cnjack/go-task(win)   
    
    go build
    ./go-task(linux) OR go-task(win)

    访问 http://127.0.0.1:3201/
    点击 "add task" 添加任务
    点击 "view the task" 查看任务列表
    添加时候默认密码为: 3201
    (我要添加密码来避免误点添加url,产生了一些奇怪的事情,ps一些服务商会访问你访问页面放置有不好的关键词.你懂的)
# powerBy
[negroni](http://github.com/codegangsta/negroni)   
![七牛](http://assets.qiniu.com/qiniu-transparent.png)   
