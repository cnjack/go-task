package app

import (
	//"git.oschina.net/cnjack/go-task/common"
	"git.oschina.net/cnjack/go-task/handler"
	"git.oschina.net/cnjack/go-task/log"
	"git.oschina.net/cnjack/go-task/task"
	"github.com/codegangsta/negroni"
	"net/http"
	"os"
)

func Run() {
	logS()
	log.InfoNoStyle("HTTP Is Running!")
	task.Init()
	log.InfoNoStyle("Task Is Inited!")

	go task.Run()
	//common.DoMySql("首次备份:")
	task.Add(43200, 2, "127.0.0.1备份:")
	httpS()
}

func logS() {
	file, _ := os.OpenFile("./log.data", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0664)
	log.SetFile(file)
	log.On()
}

func httpS() {
	n := negroni.New(negroni.NewRecovery(), negroni.NewStatic(http.Dir("public")))
	mux := http.NewServeMux()
	mux.HandleFunc("/", handler.IndexHandler)
	mux.HandleFunc("/tasklist", handler.TaskListHandler)
	mux.HandleFunc("/addtask", handler.AddTaskHandler)
	mux.HandleFunc("/addtask_do", handler.AddTaskDoHandler)
	mux.HandleFunc("/deltask", handler.DelTaskHandler)

	n.UseHandler(mux)
	n.Run(":3201")
}
